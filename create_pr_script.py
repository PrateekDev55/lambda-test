import os
import requests
import json

# Set your credentials and repository info
BITBUCKET_USERNAME = 'PrateekDev55'
BITBUCKET_APP_PASSWORD = 'ATBB4RZbJcAtESRsUMuwnRZmd99F95169EC1'
BITBUCKET_WORKSPACE = 'PrateekDev55'
REPO_SLUG = 'lambda-test'

# Function to create PR
def create_pr(from_branch, to_branch):
    url = f"https://api.bitbucket.org/2.0/repositories/{BITBUCKET_WORKSPACE}/{REPO_SLUG}/pullrequests"
    headers = {
        "Content-Type": "application/json"
    }
    payload = {
        "title": f"Auto PR from {from_branch} to {to_branch}",
        "source": {
            "branch": {
                "name": from_branch
            }
        },
        "destination": {
            "branch": {
                "name": to_branch
            }
        }
    }
    response = requests.post(url, auth=(BITBUCKET_USERNAME, BITBUCKET_APP_PASSWORD), headers=headers, data=json.dumps(payload))
    if response.status_code == 201:
        print(f"Pull request from {from_branch} to {to_branch} created successfully.")
    else:
        print(f"Failed to create pull request: {response.status_code} - {response.text}")

# Get the hotfix branch name from the pipeline environment variable
hotfix_branch = os.getenv('BITBUCKET_BRANCH')

# Check if we have a valid branch name
if hotfix_branch and hotfix_branch.startswith('hotfix-'):
    # Create PRs for both master and dev branches
    create_pr(hotfix_branch, 'master')
    create_pr(hotfix_branch, 'dev')
else:
    print("Error: This pipeline should only be triggered for hotfix branches.")
